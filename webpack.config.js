// 'use strict';
// // npm install line: auth0-lock transform-loader json-loader brfs packageify ejsify
// // var webpack = require('webpack')
// // require('dotenv').config();

// var path = require('path');
// var buildPath = path.resolve(__dirname, 'build', 'dev', 'src', 'webpack' );
// var entryPath = path.resolve(__dirname, 'src', 'webpack', 'app');

// var config = {
//   devtool: "source-map",
//   context: __dirname,
//   entry: entryPath,
//   output: {
//     path: buildPath,
//     filename: 'webpack.js'
//   },
//   module: {
//     loaders: [{
//       test: /node_modules[\\\/]auth0-lock[\\\/].*\.js$/,
//       loaders: [
//         'transform-loader/cacheable?brfs',
//         'transform-loader/cacheable?packageify'
//       ]
//     }, {
//       test: /node_modules[\\\/]auth0-lock[\\\/].*\.ejs$/,
//       loader: 'transform-loader/cacheable?ejsify'
//     }, {
//       test: /\.json$/,
//       loader: 'json-loader'
//     }]
//   }
// };

// module.exports = config;
