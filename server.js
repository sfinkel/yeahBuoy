// =======================
// get the packages we need ============
// =======================
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');

var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file
var User = require('./app/models/user'); // get our mongoose model
var Beacon = require('./app/models/Beacon'); // get our mongoose model
var Location = require('./app/models/BeaconLocation')
var BeaconSave = require('./app/models/BeaconSave')
var request = require('request');
var jwtAuth = require('express-jwt')
var dotenv = require('dotenv');
var cors = require('cors');
var jwt_decode = require('jwt-decode')
var jwtSimple = require('jwt-simple');
var FeedParser = require('feedparser')

//loads environment variables on to process.env
dotenv.load();

//Middleware to decode our token
var authorizeAndSetReqUser = jwtAuth({
    secret: new Buffer(process.env.AUTH0_CLIENT_SECRET, 'base64'),
    audience: process.env.AUTH0_CLIENT_ID,
    credentialsRequired: true,
    getToken: function fromHeaderOrQuerystring(req) { //send as header or on query string as 'token=''
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
});




// =======================
// configuration =========
// =======================

var port = process.env.PORT || 8080; // used to create, sign, and verify tokens
var db = mongoose.connect(config.database); // connect to database



//Converter Class
var Converter = require("csvtojson").Converter;



//Allowing all requests for now
var allowCrossDomain = function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Content-Type');

        next();
    }
    //use cors
app.use(cors());


// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(allowCrossDomain);

// use morgan to log requests to the console
app.use(morgan('dev'));


// =======================
// routes ================
// =======================
// basic route


// get an instance of the router for api routes
var apiRoutes = express.Router();
// app.use('/secured/ping', jwtCheck);

app.use(express.static('build/dev/src/'));
app.use(express.static('build/dev/src/img'));
app.get('/', function(res, req) {

})

//takes in a db schema and removes all elements from it
function removeAllSchema(p_Schema) {
    p_Schema.remove({}, function(err, success) {
        console.log(err);
        console.log(success);
    })
}

//removes specifically the beacon schema and location schema
function removeBeaconAndLocation() {

    removeAllSchema(Beacon);
    removeAllSchema(Location);

}

//external api to have the server go and fetch new beacon data
app.post('/update', function(req, res) {

    removeBeaconAndLocation();
    newResults();
    res.json({ success: 'triggered update' })


})

//used in development to decode a user's token
apiRoutes.post('/verify', function(req, res) {
    var decoded = getTokenDecoded(req.body.token || req.query.token || req.headers['x-access-token']);
    console.log(decoded);
    if (decoded) {
        res.json(decoded);
    } else {
        res.json({ success: false });
    }
})

// API ROUTES -------------------
// we'll get to these in a second


//NOT USED ANYMORE, BUT GOOD FOR REFERENCE
//route to authenticate a user (POST http://localhost:8080/api/authenticate)
// app.post('/authenticate', function(req, res) {

//     // find the user
//     User.findOne({
//         name: req.body.name
//     }, function(err, user) {

//         if (err) throw err;

//         if (!user) {
//             res.json({ success: false, message: 'Authentication failed. User not found.' });
//         } else if (user) {

//             // check if password matches
//             if (user.password != req.body.password) {
//                 res.json({ success: false, message: 'Authentication failed. Wrong password.' });
//             } else {
//                 console.log(user);
//                 var token = jwt.sign({ user: { _id: user._id } }, process.env.AUTH0_CLIENT_SECRET, {
//                     expiresIn: 1440 // expires in 24 hours
//                 });


//                 var decoded = getTokenDecoded(token);
//                 console.log(decoded);

//                 // return the information including token as JSON
//                 res.json({
//                     success: true,
//                     message: 'Enjoy your token!',
//                     token: token
//                 });
//             }

//         }

//     });
// });

//api to return all users, good for dev
// apiRoutes.get('/users', function(req, res) {
//     User.find({}, function(err, users) {
//         res.json(users);
//     });
// });




// // route middleware to verify a token
apiRoutes.use(authorizeAndSetReqUser);


//Middleware to check if beacon is in db and attach to req
apiRoutes.param('beacon', function(request, response, next, beacon) {
    console.log('we here: with beacon:' + beacon);
    // ... Perform database query and
    // ... Store the user object from the database in the req object
    Beacon.findOne({ 'data.#STN': beacon }, function(err, beaconId) {
        //console.log('beacon boys' + JSON.parse(beacon.buoys);
        if (err) console.log('erre');

        if (!beaconId) {
            response.json({ success: false, message: 'No beacon found' });
        } else {
            //res.json(user.buoys);
            request.beacon = beaconId;
            return next();
        }

    });
});


//api to remove a specified beacon for user
apiRoutes.post('/beacon/remove/:beacon', authorizeAndSetReqUser, function(req, res) {
    var _beaconId = req.beacon.data['#STN'];
    User.findOneAndUpdate({ user_id: req.user.sub }, { "$pull": { "beacons": _beaconId } }, { upsert: true }, function(err, numAffected) {
        // Do something after update here
        if (err) {
            console.log(err);
            res.json(null)
        } else {
            console.log({ success: numAffected })
            res.json({ success: true });
        }

    });
})

//api to save a specified beacon for user
apiRoutes.post('/beacon/save/:beacon', authorizeAndSetReqUser, function(req, res) {

    console.log('da beacon' + req.beacon.data['#STN']);
    var _beaconId = req.beacon.data['#STN'];
    console.log(_beaconId);

    console.log(req.user);

    User.findOneAndUpdate({ user_id: req.user.sub }, { "$addToSet": { "beacons": _beaconId } }, { upsert: true }, function(err, numAffected) {
        // Do something after update here
        if (err) {
            console.log(err);
            res.json({ success: 'false' });
        }
        console.log({ success: numAffected })
        res.json({ success: true });
    });

})

//return all beacons
apiRoutes.get('/beacons', function(req, res) {
    Beacon.find({}, function(err, beacons) {
        var _allBeacons = beacons.map(function(doc) {
            return doc.data;
        })
        if (_allBeacons) {
            res.json(_allBeacons);
        } else {
            res.json(null)
        }

    });
});

//return all beacons for specific user
apiRoutes.get('/user/beacons/', authorizeAndSetReqUser, function(req, res) {
    console.log(JSON.stringify(req.user.sub))
    User.findOne({ user_id: req.user.sub }, function(err, result) {
        if (err) {
            return res.json(null);
        }

        if (!result || !result.beacons) {
            return res.json(null);
        } else {
            console.log(result);
            res.json(result.beacons);
        }
    })
});


//Find information about beacons within a radius of a LON/LAT
//?lon=yourLon&lat=yourLat&radius=20
//returns beacons objects found within radius
//if radius not specified, will use 100 NM.
apiRoutes.get('/beacon/location/', function(req, res) {

    var _radius = 100;
    if (req.query.lat && req.query.lon) {
        console.log('LAT: ' + req.query.lat);
        console.log('LON: ' + req.query.lon);
        if (req.query.radius) {
            _radius = req.query.radius;
        }

        Location.find({ loc: { $near: { $geometry: { type: "Point", coordinates: [req.query.lon, req.query.lat] }, $maxDistance: _radius * 1852 } } }, function(err, locations) {
            //  console.log(err || locations);
            if (err) {
                return res.json(null);
            }
            res.json(locations);
        })

    } else {
        console.log('Something went wrong in /beacon/location/ ');
        res.json(null);
    }

});

//return beacon data object for specified beacon
apiRoutes.get('/beacon/:beacon', function(req, res) {
    if (req.beacon.data) {
        return res.json(req.beacon.data);
    } else {
        res.json(null);
    }

});

//Fetches XML data from NOAA about specific beacon
//?beacon=PTBM6
apiRoutes.get('/xml/beacon', function(req, res) {
    console.log(req.query.beacon);
    var _beaconId = ('' + req.query.beacon + '').toLowerCase();
    console.log('http://www.ndbc.noaa.gov/data/latest_obs/' + _beaconId + '.rss');

    var stream = this;
    var feedparser = new FeedParser();

    var req = request('http://www.ndbc.noaa.gov/data/latest_obs/' + _beaconId + '.rss');

    req.on('error', function(error) {
        // handle any request errors
        console.log('error with xml: ' + _beaconId);
    });

    req.on('response', function(res) {
        var stream = this;

        if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));

        stream.pipe(feedparser);
    });

    feedparser.on('error', function(error) {
        // always handle errors
    });
    feedparser.on('readable', function() {
        // This is where the action is!
        var stream = this,
            meta = this.meta // **NOTE** the "meta" is always available in the context of the feedparser instance
            ,
            item;

        while (item = stream.read()) {
            console.log(item);

        }
        if (meta) {
            res.json(meta);
        } else {
            res.json({ success: false })
        }
    });

});




app.use('/api', apiRoutes);

// =======================
// start the server ======
// =======================
app.listen(port);
console.log('Magic happens at http://localhost:' + port);


//convert NOAA's TSV file in to our database schemas
function convertText(p_text) {
    var converter = new Converter({ constructResult: false }); //for big csv data
    converter.fromString(p_text, function(err, result) {
        if (err) {
            console.log(err);
        }

    });
    converter.on("record_parsed", function(jsonObj) {
        // create a sample user
        //console.log(jsonObj);
        var _beacon = new Beacon({
            data: jsonObj
        });

        // save the sample user
        _beacon.save(function(err) {
            if (err) throw err;

            console.log('Beaon saved successfully');
            //res.json({ success: true });
        });

        console.log(jsonObj);

        var _coordinateArray = [];
        console.log('LAT' + jsonObj['LAT'])
        console.log('LON' + jsonObj['LON'])

        _coordinateArray.push(jsonObj['LON']);
        _coordinateArray.push(jsonObj['LAT']);
        console.log(_coordinateArray);

        console.log(jsonObj['#STN'])

        var _location = new Location({
            '#STN': jsonObj['#STN'],
            data: jsonObj,
            loc: {
                type: "Point",
                coordinates: _coordinateArray
            }
        })

        // save the sample user
        _location.save(function(err) {
            if (err) throw err;

            console.log('location saved successfully');
            //res.json({ success: true });
        });
        return { success: 'updated' };

    });
}


//Makes the request to go and get NOAA's current TSV and then calls the function
//which inputs that data in to the database using the database schemas
function newResults() {

    request('http://www.ndbc.noaa.gov/data/latest_obs/latest_obs.txt', function(error, response, body) {
        if (!error && response.statusCode == 200) {
            //console.log(body); // Show the HTML for the Google homepage.
            var _str = body.replace(/[ \t\r]+/g, ",");


            var lines = _str.split('\n');
            //var _headerOne = lines.splice(0,1);
            var _headerTwo = lines.splice(1, 2);
            var newtext = lines.join('\n');
            if (newtext) {
                convertText(newtext);

            }
        }
    })

}
