var os = require('os');
var gulp = require('gulp');
var opn = require('opn');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var htmlreplace = require('gulp-html-replace');
var livereload = require('gulp-livereload');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var del = require('del');
var gulpif = require('gulp-if');
var zip = require('gulp-zip');
var webpack = require('webpack-stream');


var config = function() {
    return {
        dev: {
            buildDir: './build/dev/src/',
            src: {
                html: {
                    targetFolder: '',
                    targetFileName: 'index.html',
                    files: [
                        'src/index.html'
                    ]
                },
                js: {
                    targetFolder: 'js/',
                    targetFileName: 'index.js',
                    files: [
                        //start with project settings
                        //                        'src/settings.js', //settings.js
                        'src/js/commonlib/livereload.js', //so we auto-refresh page when we save a file in /src

                        //start with common library functions
                        'src/js/commonlib/domCommon.js',

                        //add jquery
                        'src/bower_components/jquery/dist/jquery.min.js',

                        //bootstrap
                        'src/bower_components/bootstrap/dist/js/bootstrap.min.js',

                        //auth0-lock
                        'src/bower_components/auth0-lock/build/auth0-lock.min.js',

                        //leaflet
                        'src/js/vendor/leaflet/leaflet.js',

                        //leaflet extra markers
                        'src/bower_components/Leaflet.extra-markers/src/assets/js/leaflet.extra-markers.js',

                        //end with our source
                        'src/js/index.js'
                    ]
                },
                css: {
                    targetFolder: 'css/',
                    targetFileName: 'index.css',
                    files: [

                        //normalize.css reset
                        'src/bower_components/normalize-css/normalize.css',
                        
                        //font awesome
                        'src/bower_components/font-awesome/css/font-awesome.min.css',   

                        //bootstrap
                        'src/bower_components/bootstrap/dist/css/bootstrap.min.css',

                        //leaflet
                        'src/css/vendor/leaflet/leaflet.css',

                        //leaflet extra markers
                        'src/bower_components/Leaflet.extra-markers/src/assets/css/leaflet.extra-markers.css',

                        //our styles
                        'src/css/index.css'
                    ]
                },
                scss: {
                    targetFolder: 'css/',
                    targetFileName: 'index.css',
                    files: [

                        //our styles
                        //'src/scss/index.scss'
                    ]
                },
                img: {
                    targetFolder: 'img/',
                    files: [
                        'src/img/**',
                        'src/bower_components/Leaflet.extra-markers/dist/img/*'
                    ]
                },
                fonts: {
                    targetFolder: 'fonts/',
                    files: [
                        'src/fonts/**',
                        'src/bower_components/font-awesome/fonts/*'
                    ]
                },
                doc: {
                    targetFolder: '',
                    files: [
                        'src/doc/**'
                    ]
                },
                webpack: {
                    targetFolder: 'webpack/',
                    targetFileName: 'webpack.js',
                    configFileName: 'webpack.config.js',
                }
            }
        },
        release: {
            buildDir: './build/release/src/',
            src: {
                html: {
                    targetFolder: '',
                    targetFileName: 'index.html',
                    files: [
                        'src/index.html'
                    ]
                },
                js: {
                    targetFolder: 'js/',
                    targetFileName: 'index.js',
                    files: [
                        //start with project settings
                        //'src/settings.js', //settings.js
                        //'src/js/commonlib/livereload.js', //so we auto-refresh page when we save a file in /src

                        //add jquery
                        //'src/bower_components/jquery/dist/jquery.min.js',

                        //start with common library functions
                        // 'src/js/commonlib/domCommon.js',
                        // 'src/js/commonlib/ajax.js',

                        //end with our source
                        'src/js/index.js'
                    ]
                },
                css: {
                    targetFolder: 'css/',
                    targetFileName: 'index.css',
                    files: [
                        //end with our styles
                        'src/css/index.css'
                    ]
                },
                scss: {
                    targetFolder: 'css/',
                    targetFileName: 'indexscss.css',
                    files: [

                        //end with our styles
                        'src/scss/index.scss'
                    ]
                },
                img: {
                    targetFolder: '',
                    files: [
                        'src/img/**'
                    ]
                },
                fonts: {
                    targetFolder: '',
                    files: [
                        'src/fonts/**'
                    ]
                },
                doc: {
                    targetFolder: '',
                    files: [
                        'src/doc/**'
                    ]
                }
            }
        }
    }

}()

//TODO documentation

var devOrRelease = 'dev';



gulp.copy = function(src, dest) {
    return gulp.src(src, { base: "." })
        .pipe(gulp.dest(dest));
};

gulp.task('build-scss', function() {
    //index.css + index.scss


    var files = config[devOrRelease].src.css.files;
    files = files.concat(config[devOrRelease].src.scss.files);


    return gulp.src(files)
        .pipe(plumber())
        .pipe(concat(config[devOrRelease].src.css.targetFileName))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(concat(config[devOrRelease].src.css.targetFileName))
        .pipe(gulpif((devOrRelease === 'release'), minifyCSS()))
        .pipe(gulp.dest(config[devOrRelease].buildDir + config[devOrRelease].src.css.targetFolder));


});


gulp.task('clean', function() {
    return del(config[devOrRelease].buildDir + '*');
});

gulp.task('build-img', function() {

    //copy the 'src/fonts' folder over to 'build/dev/src/fonts'
    return gulp.src(config[devOrRelease].src.img.files)
        .pipe(gulp.dest(config[devOrRelease].buildDir + config[devOrRelease].src.img.targetFolder));
})

gulp.task('build-fonts', function() {

    //copy the 'src/fonts' folder over to 'build/dev/src/fonts'
    return gulp.src(config[devOrRelease].src.fonts.files)
        .pipe(gulp.dest(config[devOrRelease].buildDir + config[devOrRelease].src.fonts.targetFolder));
})

gulp.task('build-js', function() {

    //takes all JS files and concats in to one, puts in buildDir as configurable 'buildFile'
    return gulp.src(config[devOrRelease].src.js.files, { base: 'src' })
        .pipe(concat(config[devOrRelease].src.js.targetFileName))
        .pipe(gulpif(devOrRelease === 'release', uglify()))
        .pipe(gulp.dest(config[devOrRelease].buildDir + config[devOrRelease].src.js.targetFolder));
});

gulp.task('build-html', function() {
    console.log("building html");
    //takes the html file array and makes in to one, should only have one html file for now
    return gulp.src(config[devOrRelease].src.html.files)
        .pipe(htmlreplace({
            'js': config[devOrRelease].src.js.targetFolder + config[devOrRelease].src.js.targetFileName,
            //'webpack': config[devOrRelease].src.webpack.targetFolder + config[devOrRelease].src.webpack.targetFileName,
            'css': config[devOrRelease].src.css.targetFolder + config[devOrRelease].src.css.targetFileName,
            'scss': {
                src: [config[devOrRelease].src.scss.targetFolder + config[devOrRelease].src.scss.targetFileName],
                tpl: '<link rel="stylesheet" href="%s">'
            }

        }))
        // .pipe(rename(config[devOrRelease].src.html.targetFileName))
        .pipe(gulp.dest(config[devOrRelease].buildDir + config[devOrRelease].src.html.targetFolder));

});


gulp.task('build-doc', function() {
    console.log("building doc");
    return gulp.src(config[devOrRelease].src.doc.files, { base: 'src' })
        .pipe(gulp.dest(config[devOrRelease].buildDir + config[devOrRelease].src.doc.targetFolder));

});
//zips the package
gulp.task('zipItZipItRealGood', function() {
    if (!devOrRelease) {
        console.log('need to be dev or release');
        return;
    }

    return gulp.src(config[devOrRelease].buildDir + '**')
        .pipe(zip('' + devOrRelease + '.zip'))
        .pipe(gulp.dest(config[devOrRelease].buildDir + '..'));
});

gulp.task('build-webpack', function() {

    var devOrRelease = 'dev';
     return gulp.src('.')
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest('build/dev/src/webpack/'));
});

//replace html srcs with dev build.js
gulp.task('build', function(callback) {

    console.log("build");

    runSequence(
        'clean',
        'build-scss',
        //'build-css',
        'build-js',
        //'build-webpack',
        'build-html',
        'build-fonts',

        'build-img',
        'build-doc',
        'zipItZipItRealGood',

        callback);
});

gulp.task('open-build', function() {
    var browser = os.platform() === 'linux' ? 'google-chrome' : (
        os.platform() === 'darwin' ? 'google-chrome' : (
            os.platform() === 'win32' ? 'chrome' : 'firefox'));
    console.log(browser);
    console.log(__dirname);
    var _fileToOpen = 'file://' + __dirname + '/' + config[devOrRelease].buildDir + config[devOrRelease].src.html.targetFileName + '';
    console.log(_fileToOpen);
    opn(_fileToOpen);

});

//replace html srcs with dev build.js
gulp.task('build-release', function(callback) {
    devOrRelease = 'release';
    console.log("build-release");

    runSequence(
        'build',
        callback);
});

//replace html srcs with dev build.js
gulp.task('build-dev', function(callback) {
    devOrRelease = 'dev';
    console.log("build-dev");

    runSequence(
        'build',
        callback);
});





gulp.task('reload', function() {
    console.log("reloading web page...")

    livereload.reload();

});

gulp.task('watch-dev', function() {
    devOrRelease = 'dev';
    livereload.listen({
        reloadPage: ".build/" + devOrRelease + "/src/index.html",
        port: 3009,
        host: "127.0.0.1"
    });

    gulp.watch('./src/**', function() {
        runSequence('build', 'reload');
    });

    runSequence('open-build', function() {
        return;
    });
});

gulp.task('watch-release', function() {
    devOrRelease = 'release';
    livereload.listen({
        reloadPage: ".build/" + devOrRelease + "/src/index.html",
        port: 3009,
        host: "127.0.0.1"
    });

    gulp.watch('./src/**', function() {
        runSequence('build', 'reload');
    });

    runSequence('open-build', function() {
        return;
    });
});
