var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

//var threeTaps = require '../modules/threeTaps';


var LocationSchema = new Schema({

    "#STN" : String,
    "type" : String,
    "code" : String,
    data: {},
    "loc" : {
      "type" : { type : String},
      "coordinates" : []
    }
});

LocationSchema.index({ loc: '2dsphere' });




mongoose.model('Location', LocationSchema);
module.exports = mongoose.model('Location');

