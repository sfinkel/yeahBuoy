// // get an instance of mongoose and mongoose.Schema
// var mongoose = require('mongoose');
// var Schema = mongoose.Schema;

// // set up a mongoose model and pass it using module.exports
// module.exports = mongoose.model('User', new Schema({
//     name: String,
//     password: String,
//     admin: Boolean
// }));

// get an instance of mongoose and mongoose.Schema
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
var _model = mongoose.model('Beacons', new Schema({
    //buoys: Object
    //data: Object,

    data : {
        "TIDE" : String,
        "VIS" : String,
        "DEWP" : String,
        "WTMP" : String,
        "ATMP" : String,
        "PTDY" : String,
        "PRES" : String,
        "MWD" : String,
        "APD" : String,
        "DPD" : String,
        "WVHT" : String,
        "GST" : String,
        "WSPD" : String,
        "WDIR" : String,
        "mm" : String,
        "hh" : String,
        "DD" : String,
        "MM" : String,
        "YYYY" : String,
        "LON" : String,
        "LAT" : String,
        "#STN" : String
    }

}));



module.exports = _model;