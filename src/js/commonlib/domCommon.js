"use strict";

//Older Internet Explorer compatible event.preventDefault
var preventDefault = function(e) {  // jshint ignore:line
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
};

//Older Internet Explorer compatible event.stopPropagation
var stopPropagation = function(e) {  // jshint ignore:line
    e = e || window.event;
    if (e.stopPropogation)
        e.stopPropogation();
    e.cancelBubble = true;
};
/* jshint ignore:start */
var KEY_CODE_ENTER = 13;
var KEY_CODE_ARROW_LEFT = 37;
var KEY_CODE_ARROW_RIGHT = 39;
var KEY_CODE_ESCAPE = 27;
/* jshint ignore:end */

//Older Internet Explorer compatible event.keyCode
function keyCode(e) {
    e = e || window.event;
    return (e.keyCode || e.which);
}

//easier to write dom functions
function byId(id) {
    return document.getElementById(id);
}

function byClass(className) {
    return document.getElementsByClassName(className);
}

function bySelectorAll(selector) {
    return document.querySelectorAll(selector);
}

function bySelector(selector) {
    return document.querySelector(selector);
}

function create(p_elementTag, p_id, p_className) {
    var _element = document.createElement(p_elementTag);
    if(p_id){
        _element.id = p_id;
    }
    else if(p_className){
        _element.className = p_className;
    }
    else{}
    return _element;
}

//Older Internet Explorer compatible event binding
function bind(element, event, callback) {
    //IE8 fallback
    element.addEventListener ? element.addEventListener(event, callback, false) : element.attachEvent('on' + event, callback);
}

//Older Internet Explorer compatible event unbinding
function unbind(element, event, callback) {
    element.removeEventListener ? element.removeEventListener(event, callback, false) : element.detachEvent('on' + event, callback);
}

//Does element have this className?
function hasClass(elem, className) {
 //   console.log("ELEM: "+elem.id+"HAS CLASS: "+elem.className);
    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}

//Add a class to an element
function addClass(elem, className) {
    if (!hasClass(elem, className)) {
        elem.className += ' ' + className;
        return true;
    }
    return false;
}

//Remove a class to an element
function removeClass(elem, className) {
    if (!elem.className) {
        //console.log("Class not found: \nElement: " + elem + " \nclassName: " + className);
        return;
    }

    var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
    if (hasClass(elem, className)) {
        while (newClass.indexOf(' ' + className + ' ') >= 0) {
            newClass = newClass.replace(' ' + className + ' ', ' ');
        }

        elem.className = newClass.replace(/^\s+|\s+$/g, '');
    }
}

//Add or remove a class based on its existence
function toggleClass(elem, className) {
    hasClass(elem, className) ? removeClass(elem, className) : addClass(elem, className);
}

//Removes a DOM node and all its children
function removeChildNodesAndNode(node) {
    while (node.hasChildNodes()) {
        removeNodeChildren(node.firstChild);
    }
    node.remove();
}

//Removes JUST a DOM node's children
function removeChildNodes(node) {
    while (node.hasChildNodes()) {
        removeNodeChildren(node.firstChild);
    }
}

//Recursive removal of children nodes
function removeNodeChildren(node) {
    while (node.hasChildNodes()) {
        removeNodeChildren(node.firstChild);
    }
    node.parentNode.removeChild(node);
    // console.log("Removed!");
}

// foreach ie8
// http://stackoverflow.com/questions/14827406
if ( !Array.prototype.forEach ) {
  Array.prototype.forEach = function(fn, scope) {
    for(var i = 0, len = this.length; i < len; ++i) {
      fn.call(scope, this[i], i, this);
    }
  }
}

// filter polyfill for IE8
// https://gist.github.com/eliperelman/1031656
if (![].filter) {
    Array.prototype.filter = function(a, b, c, d, e) {
        /* jshint -W030 */
        c = this;
        d = [];
        for (e = 0; e < c.length; e++)
            a.call(b, c[e], e, c) && d.push(c[e]);
        return d;
    };
}


//function to simulate clicking on an id
function triggerClick(id) {
    var event = new MouseEvent('click', {
        'view': window,
        'bubbles': true,
        'cancelable': true
    });

    var cb = document.getElementById(id);
    var canceled = !cb.dispatchEvent(event);
    if (canceled) {
        // A handler called preventDefault.
        //alert("canceled");
    } else {
        // None of the handlers called preventDefault.
        //alert("not canceled");
    }
}

function toFixed(value, precision) {
    var precision = precision || 0,
        power = Math.pow(10, precision),
        absValue = Math.abs(Math.round(value * power)),
        result = (value < 0 ? '-' : '') + String(Math.floor(absValue / power));

    if (precision > 0) {
        var fraction = String(absValue % power),
            padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');
        result += '.' + padding + fraction;
    }
    return result;
}



