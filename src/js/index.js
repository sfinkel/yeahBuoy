/****  SORRY ^ THAT'S MY COMMON STUFF  (GULP) ****/
/****        FUN STUFF STARTS HERE            ****/

//variables to be used with auth0 lock
var Auth0Variables = {
    AUTH0_CLIENT_ID: 'za5pPf52dBsHUduuP5OWRYuYIYofYHBQ',
    AUTH0_DOMAIN: 'mrfinks.auth0.com'
};

$(document).ready(function() {

    //the domain we are currently at
    var _domain = window.location.origin;

    //Construct a new auth0 lock with our auth0 client id and auth0 domain variables for this app
    var lock = new Auth0Lock(
        Auth0Variables.AUTH0_CLIENT_ID,
        Auth0Variables.AUTH0_DOMAIN
    );

    //used for setting the user profile from auth0 lock
    var userProfile;


    //our DOM ID for the map
    var _leafletMapDomId = 'mapid';

    //reference to our map
    var _leafletMap = L.map(_leafletMapDomId);
    
    //set a starting map view
    setMapView([40, -73], 2);
    L.Icon.Default.imagePath = 'img';
    // _leafletMap.setMaxBounds([
    //     [84.67351256610522, -174.0234375],
    //     [-58.995311187950925, 223.2421875]
    // ]);

    //set up the options for our map layer
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
        continuousWorld: false,
        noWrap: true
    }).addTo(_leafletMap);

    //Adding a sea layer to the map
    L.tileLayer('http://tiles.openseamap.org/seamark/{z}/{x}/{y}.png', {
        attribution: 'Map data: &copy; <a href="http://www.openseamap.org">OpenSeaMap</a> contributors',
        //do these apply to this layer?
        continuousWorld: false,
        noWrap: true
    }).addTo(_leafletMap);


    var _beaconLayer = L.featureGroup().addTo(_leafletMap);


    //this is what we expect our beacon object to look like
    //attaching some GUI texts for describing the database key and units
    //Easier to understand wspd as WIND SPEED on the GUI
    var _beaconSchema = {
        id: { dbKey: '#STN', guiText: 'STATION #', unit: '' },
        lon: { dbKey: 'LON', guiText: 'LONGITUDE', unit: 'deg' },
        lat: { dbKey: 'LAT', guiText: 'LATITUDE', unit: 'deg' },
        tide: { dbKey: 'TIDE', guiText: 'WATER LEVEL', unit: 'ft' },
        vis: { dbKey: 'VIS', guiText: 'STATION VISIBILITY', unit: 'nmi' },
        dewp: { dbKey: 'DEWP', guiText: 'DEWPOINT TEMPERATURE', unit: 'degC' },
        wtmp: { dbKey: 'WTMP', guiText: 'SEA SURFACE TEMPERATURE', unit: 'degC' },
        atmp: { dbKey: 'ATMP', guiText: 'AIR TEMPERATURE', unit: 'degC' },
        ptdy: { dbKey: 'PTDY', guiText: 'PRESSURE TENDENCY', unit: 'hPa' },
        pres: { dbKey: 'PRES', guiText: 'SEA LEVEL PRESSURE', unit: 'hPa' },
        mwd: { dbKey: 'MWD', guiText: 'WAVE DIRECTION @ DOMINANT PERIOD', unit: 'DTN' },
        apd: { dbKey: 'APD', guiText: 'AVERAGE WAVE PERIOD', unit: 'sec' },
        dpd: { dbKey: 'DPD', guiText: 'DOMINANT WAVE PERIOD', unit: 'sec' },
        wvht: { dbKey: 'WVHT', guiText: 'SIGNIFICANT WAVE HEIGHT', unit: 'm' },
        gst: { dbKey: 'GST', guiText: 'PEAK 5 OR 8 SECOND GUST SPEED', unit: 'm/s' },
        wspd: { dbKey: 'WSPD', guiText: 'WIND SPEED', unit: 'm/s' },
        wdir: { dbKey: 'WDIR', guiText: 'WIND DIRECTION', unit: 'DTN' },
        mm: { dbKey: 'mm', guiText: 'MINUTES', unit: 'minutes' },
        hh: { dbKey: 'hh', guiText: 'HOURS', unit: 'hours' },
        dd: { dbKey: 'DD', guiText: 'DAY', unit: 'day' },
        MM: { dbKey: 'MM', guiText: 'MONTH', unit: 'month' },
        YYYY: { dbKey: 'YYYY', guiText: 'YEAR', unit: 'year' },
        TIME: { dbKey: 'TIME', guiText: 'TIME', unit: '' },
        DATE: { dbKey: 'DATE', guiText: 'DATE', unit: '' },
        NAME: { dbKey: 'NAME', guiText: 'NAME', unit: '' }

    }

    //mapping of the schema above to be found by dbKey instead of original key
    var _beaconSchemaByDbKey = function() {
        var _newObj = {};
        for (key in _beaconSchema) {

            var _theDbKey = _beaconSchema[key].dbKey;
            _newObj[_theDbKey] = _beaconSchema[key];
            _newObj[_theDbKey]['identity'] = key;

        }
        return _newObj;
    }()


    //send the token with every request
    $.ajaxSetup({
        'beforeSend': function(xhr) {
            if (localStorage.getItem('userToken')) {
                //console.log('token is: ' + localStorage.getItem('userToken'))
                xhr.setRequestHeader('Authorization',
                    'Bearer ' + localStorage.getItem('userToken'));
            }
        }
    });


    //Function to remove a user beacon based on passed in beacon id
    //Attach this function to the window as it will need to be accessed from the marker
    //If we ever want to modularize this, letting this be known explicitly
    window.removeBeacon = function(p_beaconId) {

        $.ajax({
            url: _domain + '/api/beacon/remove/' + p_beaconId,
            method: 'POST'
        }).then(function(data, textStatus, jqXHR) {
            //console.log('success post' + data);
            if (data.success) {
                alert('Beacon: ' + p_beaconId + ' has been removed.');
                getUserBeacons();
            } else {
                console.log('response given from server, but error on server side removing beacon');
            }
        }, function() {
            console.log('error with removing beacon: ' + p_beaconId)
            getUserBeacons();
        });

    }

    //Function to save a user beacon based on passed in beacon id
    //Attach this function to the window as it will need to be accessed from the marker
    //If we ever want to modularize this, letting this be known explicitly
    window.saveBeacon = function(p_beaconId) {

        $.ajax({
            url: _domain + '/api/beacon/save/' + p_beaconId,
            method: 'POST'
        }).then(function(data, textStatus, jqXHR) {
            if (data.success) {
                alert('YEAH BUOY-EEE!  Beacon: ' + p_beaconId + ' has been saved.');
            } else {
                console.log('response given from server, but error on server side saving beacon');
            }
        }, function() {
            console.log('error with saving beacon: ' + p_beaconId)
        });

    }

    //function to return an ExtraMarkers color (css) based on temperature
    //fahrenheit passed in
    function tempFToClass(p_temp) {

        if (p_temp >= 80) {
            return 'orange-dark';
        } else if (p_temp >= 73) {
            return 'red';
        } else if (p_temp >= 69) {
            return 'orange';
        } else if (p_temp >= 66) {
            return 'yellow';
        } else if (p_temp >= 63) {
            return 'cyan';
        } else if (p_temp >= 60) {
            return 'blue';
        } else if (p_temp >= 58) {
            return 'blue-dark';
        } else if (p_temp < 55) {
            return 'violet';
        } else {
            return 'white';
        }
    }

    //Conversion from input celsius to fahrenheit, return fahrenheit
    function convertCelsiusToFahrenheit(p_celsius) {
        return parseFloat(p_celsius * 9 / 5 + 32)
    }

    //Plot a beacon object on the map
    function plotBeacon(p_beaconObject) {

        console.log(p_beaconObject)

        //get the latitude
        var _lat = toFixed(parseFloat(p_beaconObject[_beaconSchema.lat.dbKey]), 2);
        //console.log("_lat is: " + _lat);

        //get the longitude
        var _lon = toFixed(parseFloat(p_beaconObject[_beaconSchema.lon.dbKey]), 2);

        //check them for validity
        if ((!($.isNumeric(_lat))) || (!($.isNumeric(_lat)))) {
            //console.log('plotBeacon: lat or lon is undefined');
            return;
        }

        //create a coordinates array from those values, will use for leaflet marker
        var _newArr = [];
        _newArr.push(_lat);
        _newArr.push(_lon);


        //grab the station id, will use for the leaflet marker
        var _str = p_beaconObject[_beaconSchema.id.dbKey];
        if (!_str) {
            console.log('plotBeacon: beaconObject #STN is blank')
        }

        //use _beaconSchema to get the proper field for beaconId
        var _beaconId = p_beaconObject[_beaconSchema.id.dbKey];


        var _object = {};
        _object[_beaconSchema.id.dbKey] = _beaconId;

        //Want to show the water surface temp as well for now
        var _beaconTemp = p_beaconObject[_beaconSchema.wtmp.dbKey];
        
        //But let's show 'N/A' if not available...MM comes from NOAA TSV
        if (!(_beaconTemp === 'MM')) {
            //conversion to _fahrenheit
            var _fahrenheit = toFixed(parseFloat(convertCelsiusToFahrenheit(_beaconTemp)), 2);
        } else {
            _fahrenheit = 'N/A';
        }

        //change the marker color based on my temperature -> color scale
        var _markerColor = tempFToClass(_fahrenheit);
        
        //make custom marker using ExtraMarkers
        var _customMarker = L.ExtraMarkers.icon({
            // icon: 'fa-coffee',  //don't need, but could use in future
            markerColor: _markerColor,
            shape: 'circle',
            // prefix: 'fa'        //don't need, but could use in future
        });

        //make the custom marker icon object
        _object['icon'] = _customMarker;

        //apply that object to leaflet using leaflet's .marker
        var marker = L.marker(_newArr, _object);

        //To be added to the leaflet marker
        var _saveBeacon = '<a href=\'#/\' onclick=window.saveBeacon(\'' + _beaconId + '\')>Save</a>';
        var _removeBeacon = '<a href=\'#/\' onclick=window.removeBeacon(\'' + _beaconId + '\')>Remove</a>';
        var _temp = '<span>Temperature: ' + _fahrenheit + '&deg; F</span>'
        
        marker.bindPopup('' + _beaconId + ' <br>'+_saveBeacon+'<br>'+_removeBeacon+'<br>'+_temp+'');
        
        //add click event handler when marker clicked to load the data for the beacon
        marker.addTo(_beaconLayer).on('click', beaconMarkerClicked);
        return p_beaconObject;


    }

    //Will the image load?
    //Success: p_success callback
    //Failure: p_err callback
    //Used to hide the image if beacon image is not given by NOAA
    function checkImage(imageSrc, p_success, p_err) {
        var img = new Image();
        img.onload = p_success;
        img.onerror = p_err;
        img.src = imageSrc;
    }

    //Event handler for a beacon marker being clicked.
    //Grabs a marker's '_stn' as beaconId off of the leaflet marker .options
    function beaconMarkerClicked(e) {

        var _stn = _beaconSchema.id.dbKey;
        getBeaconInfo(this.options[_stn], populateBeaconInfo);


        var _stn = '' + this.options[_stn] + '';

        //does the image load?
        checkImage('http://www.ndbc.noaa.gov/images/stations/' + _stn.toLowerCase() + '_mini.jpg',
            function(p_src) {
                //success
                byId('beaconData-image').src = this.src;
                byId('beaconData-image').style.display = 'block';
            },
            function() {
                //error
                byId('beaconData-image').style.display = 'none';
            });

        return false;


    }


    //Function to grab XML data for a specific beaconId
    //Takes a callback as p_cb parameter and is fired when ajax data is returned
    function getXmlDataForBeaconId(p_beaconId, p_cb, p_err) {
        console.log('we here');
        $.ajax({
            url: _domain + '/api/xml/beacon?beacon=' + p_beaconId,
            method: 'GET'
        }).then(function(data, textStatus, jqXHR) {

            console.log(data);
            if ((p_cb) && !(typeof p_cb === 'function')) {
                console.log('getBeaconInfo: passed callback is not a function');
                return;
            } else {
                p_cb(data);
            }

            //plotBeacon(data);

        }, function() {
            console.log('no beacon info found for: ' + p_beaconObject[_beaconSchema.id.dbKey])
            if ((p_err) && !(typeof p_err === 'function')) {
                console.log('getBeaconInfo: passed p_err callback is not a function');
                return;
            } else {
                p_err();
            }

            //alert('You need to download the server seed and start it to call this API');
        });
    }




    //Function to populate the GUI with the info of the current beacon selected
    //Mapping class to data
    //i.e. in DOM  : 
    //beaconData-id class for #STN
    //beaconDescription-id class for STATION #
    function populateBeaconInfo(p_beaconObject) {
        //$mwo.trigger('pause');
        var _classDataPrefix = 'beaconData';
        var _classDescriptionPrefix = 'beaconDescription';
        console.log(_beaconSchemaByDbKey);
        manipulateDataInBeaconObject(p_beaconObject);
        console.log(p_beaconObject)

        for (key in p_beaconObject) {
            $('.' + _classDescriptionPrefix + '-' + _beaconSchemaByDbKey[key].identity).text(_beaconSchemaByDbKey[key].guiText);
            console.log('set: ' + _beaconSchemaByDbKey[key].identity + ' with: ' + _beaconSchemaByDbKey[key].guiText)
            if (p_beaconObject[key] === 'MM'  || !p_beaconObject[key]) {
                $('.' + _classDataPrefix + '-' + _beaconSchemaByDbKey[key].identity).text('N/A');
                console.log('set: ' + _beaconSchemaByDbKey[key].identity + ' with value: N/A');
            } else {
                $('.' + _classDataPrefix + '-' + _beaconSchemaByDbKey[key].identity).text(p_beaconObject[key] + ' ' + _beaconSchemaByDbKey[key].unit);
                console.log('set: ' + _beaconSchemaByDbKey[key].identity + ' with: ' + _beaconSchemaByDbKey[key].guiText)
                console.log('set: ' + _beaconSchemaByDbKey[key].identity + ' with value: ' + p_beaconObject[key])
            }
            // console.log(_beaconSchemaByDbKey[key].identity+" is: "+p_beaconObject[key]);
            // console.log(_beaconSchemaByDbKey[key].identity +'is: '+_beaconSchemaByDbKey[key].guiText);
        }


        //a function to take a beaconId and set the NAME key in the _beaconSchema
        getXmlDataForBeaconId(p_beaconObject[_beaconSchema.id.dbKey], function(p_rssObject) {
            console.log('is: ' + p_rssObject.title);
            if (p_rssObject && p_rssObject.title) {
                $('.' + _classDescriptionPrefix + '-NAME').text('NAME');
                $('.' + _classDataPrefix + '-NAME').text(p_rssObject.title);
            }
        }, function() {
            console.log('no xml data for: ' + p_beaconObject[_beaconSchema.id.dbKey] + ' found.');

        })

    }

    //Sometimes we want to change the data received from beaconObject
    //For instance, find degC data in object and convert to fahrenheit.
    //This can be seen as middleware to the populateBeaconInfo function
    function manipulateDataInBeaconObject(p_beaconObject) {

        for (key in p_beaconObject) {
            if (_beaconSchemaByDbKey[key].unit == 'degC') {
                var _fahrenheit = convertCelsiusToFahrenheit(p_beaconObject[key]);
                p_beaconObject[key] = _fahrenheit;
            }
        }

        var _beaconMinutes = p_beaconObject[_beaconSchema.mm.dbKey];
        var _beaconHours = p_beaconObject[_beaconSchema.hh.dbKey];
        var _beaconDay = p_beaconObject[_beaconSchema.dd.dbKey];
        var _beaconMonth = p_beaconObject[_beaconSchema.MM.dbKey];
        var _beaconYear = p_beaconObject[_beaconSchema.YYYY.dbKey];
        var _timeFormat = _beaconHours + ":" + _beaconMinutes;
        var _dateFormat = _beaconYear + "-" + _beaconMonth + "-" + _beaconDay;

        p_beaconObject['TIME'] = _timeFormat;
        p_beaconObject['DATE'] = _dateFormat;

    }

    //function to take a beacon id as p_beaconId and a callback p_cb
    //Callback is called upon receiving of data
    function getBeaconInfo(p_beaconId, p_cb) {
        if (!p_beaconId) {
            //console.log('getBeaconInfo: no beacon passed');
            return;
        }
        if ((p_cb) && !(typeof p_cb === 'function')) {
            console.log('getBeaconInfo: passed callback is not a function');
            return;
        }

        //console.log('p_beaconId is ' + p_beaconId);
        $.ajax({
            url: _domain + '/api/beacon/' + p_beaconId,
            method: 'GET'
        }).then(function(data, textStatus, jqXHR) {
            p_cb(data);
        }, function() {
            console.log('no beacon info found for: ' + p_beaconId + ' Callback not fired.')
            return null;
            //alert('You need to download the server seed and start it to call this API');
        });
    }


    //refresh the map by removing the current beacon layer on map and adding a new one 
    function clearMarkersFromMap() {
        _leafletMap.removeLayer(_beaconLayer);
        _beaconLayer = L.featureGroup().addTo(_leafletMap);
    }


    //function get all beacons and plot them on a map
    function getAllBeacons() {
        clearMarkersFromMap();
        // Just call your API here. The header will be sent
        $.ajax({
            url: _domain + '/api/beacons',
            method: 'GET'
        }).then(function(data, textStatus, jqXHR) {
            //console.log(jqXHR);
            //console.log(data);
            clearMarkersFromMap();
            if (!data || !data.length) {
                console.log('no beacons found?');
            } else {
                for (var _i = 0; _i < data.length; _i++) {
                    plotBeacon(data[_i])
                }
            }

            //alert('The request to the secured enpoint was successfull');
            //console.log(data);

        }, function() {
            //alert('You need to download the server seed and start it to call this API');
        });

    }

    //function get all of current user's beacons and plot them on a map
    function getUserBeacons() {
        //console.log('get user beacons')
        // Just call your API here. The header will be sent
        $.ajax({
            url: _domain + '/api/user/beacons',
            method: 'GET'
        }).then(function(data, textStatus, jqXHR) {
                //console.log(jqXHR);
                //console.log(data);
                clearMarkersFromMap();
                if (!data || !data.length) {
                    console.log('Do you have any beacons saved? Find a beacon, click it, and save it.')

                } else {
                    for (var _i = 0; _i < data.length; _i++) {
                        getBeaconInfo(data[_i], plotBeacon);
                    }

                }
            },
            function() {
                console.log('getUserBeacons ajax error');
                //alert('You need to download the server seed and start it to call this API');
            });

    }

    //Using open streetmap to search, find a place, and use first result
    function findPlace(p_placeString, p_radius) {
        if (!p_placeString) {
            //console.log('you should enter a place like 92648');
        }
        if (!p_radius) {
            p_radius = 20;
        }
        $.get('https://nominatim.openstreetmap.org/search?format=json&q=+' + p_placeString + '').done(function(data) {
            //console.log(data);
            if (data.length > 0) {
                //after grabbing LON/LAT, do our searchByLonLat which will plot the result 
                searchByLonLat(data[0].lon, data[0].lat, p_radius, true);
            } else {
                //console.log('no place found');
            }
        })


    };

    //set the map view to p_longLatArray coordinates array at zoom level p_zoom
    function setMapView(p_longLatArray, p_zoom) {
        _leafletMap.setView(p_longLatArray, p_zoom);
    }

    //Generic function to search by LON/LAT and plot on map.  
    //Optional zoomInOnMarker param to zoom to last marker
    function searchByLonLat(p_longitude, p_latitude, _radius, p_zoomInOnMarker) {
        $.ajax({
            url: _domain + '/api/beacon/location' + '?lon=' + p_longitude + '&lat=' + p_latitude + '&radius=' + _radius,
            method: 'GET'
        }).then(function(data, textStatus, jqXHR) {
            //console.log(jqXHR);
            //console.log(data);
            clearMarkersFromMap();
            var _lastBeacon = null;
            if (!data.length) {
                console.log('no beacons found for this location');
                return;
            }
            for (var _i = 0; _i < data.length; _i++) {
                _lastBeacon = plotBeacon(data[_i].data);
            }
            if (_lastBeacon && p_zoomInOnMarker) {
                //console.log('last beacon is: ' + _lastBeacon);
                setMapView([_lastBeacon[_beaconSchema.lat.dbKey], _lastBeacon[_beaconSchema.lon.dbKey]], 5);
            } else {
                console.log('_lastBeacon or p_zoomInOnMarker = null');
            }

        }, function() {
            console.log('err response from server for searchByLonLat');
            //alert('You need to download the server seed and start it to call this API');
        });
    }


    //function to launch us in to full screen
    function launchIntoFullscreen(element) {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    }


    //BINDS


    //when the login button is clicked, clear the AUTH0 lock, set the token, and set the profile
    $('.btn-login').click(function(e) {
        e.preventDefault();
        lock.showSignin(function(err, profile, token) {
            if (err) {
                // Error callback
                //console.log('There was an error');
                alert('There was an error logging in');
            } else {
                // Success calback

                // Save the JWT token.
                localStorage.setItem('userToken', token);

                // Save the profile
                userProfile = profile;

                $('.login-box').hide();
                $('.logged-in-box').show();
                $('.nickname').text(profile.nickname);
                $('.nickname').text(profile.name);
                $('.avatar').attr('src', profile.picture);

                //to refresh the map for some reason needs to be used with jquery show/hide
                _leafletMap.invalidateSize(true);
                getUserBeacons();
            }
        });
    });

    //when the search button is clicked, grab the lat/lon and radius, and call find it & map it
    $('.btn-search-place').click(function(e) {
        findPlace($("#place-input").val(), $("#place-radius-input").val());
        e.preventDefault();
    });

    //button to draw all user's beacons on the map
    $('.btn-api-user-beacons').click(function(e) {
        getUserBeacons();
        e.preventDefault();

    });

    //button to draw all beacons on the map
    $('.btn-api-all-beacons').click(function(e) {
        getAllBeacons();
        e.preventDefault();

    });

    //to logout the user
    $('.btn-logout').click(function(e) {

        //use the domain we are currently at
        window.location = 'https://' + Auth0Variables.AUTH0_DOMAIN + '/v2/logout?returnTo=http://vast-coast-33220.herokuapp.com/&client_id=' + Auth0Variables.AUTH0_CLIENT_ID + '';

    })

    //when the GO button is pressed, grab LON/LAT values & optional radius and search
    $('.search-lon-lat').click(function(e) {

        var _lat = toFixed(parseFloat($('#lat-input').val()), 2);
        var _lon = toFixed(parseFloat($('#lon-input').val()), 2);
        var _radius = parseInt($('#radius-input').val());


        if (!_lat || !_lon) {
            alert('you did not enter lon or lat?');
        }
        if (!_radius) {
            //console.log('radius-input not entered, will use default radius');
        }

        //search by LON/LAT
        searchByLonLat(_lon, _lat, _radius, true);
        e.preventDefault();
    });

    //call the server to update data, doing so will cause a minute wait or so while db is repopulated
    $('.btn-api-update').click(function(e) {

        $.ajax({
            url: _domain + '/update',
            method: 'POST'
        }).then(function(data, textStatus, jqXHR) {

            //alert('The request to the secured enpoint was successfull');
            console.log(data);

        }, function() {
            //alert('You need to download the server seed and start it to call this API');
        });
        e.preventDefault();
    });

    //send map in to fullscreen mode
    $('.btn-map-fullscreen').click(function(e) {
        launchIntoFullscreen(byId(_leafletMapDomId));
        e.preventDefault();
    });


    //show me beacons within 50 nm of where I click on the map
    _leafletMap.on('click', function(e) {
        searchByLonLat(e.latlng.lng, e.latlng.lat, 50, false);
    });


});
