// var Auth0Variables = require('./auth0-variables');
// var Auth0Lock = require('auth0-lock');
// //var $ = require('jquery');
// var L = require('leaflet');


// $(document).ready(function() {

//     var domain = window.location.origin;
//     var mapid = L.map('mapid').setView([40, -73], 2);
//     L.Icon.Default.imagePath = 'img';

//     L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
//         attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
//         continuousWorld: true,
//         noWrap: true
//     }).addTo(mapid);

//     var _beaconLayer = L.featureGroup().addTo(mapid);

//     var lock = new Auth0Lock(
//         Auth0Variables.AUTH0_CLIENT_ID,
//         Auth0Variables.AUTH0_DOMAIN
//     );

//     var userProfile;

//     var _beaconSchema = {
//         id: { dbKey: '#STN', guiText: 'STATION #', unit: '' },
//         lon: { dbKey: 'LON', guiText: 'LONGITUDE', unit: 'deg' },
//         lat: { dbKey: 'LAT', guiText: 'LATITUDE', unit: 'deg' },
//         tide: { dbKey: 'TIDE', guiText: 'WATER LEVEL', unit: 'ft' },
//         vis: { dbKey: 'VIS', guiText: 'STATION VISIBILITY', unit: 'nmi' },
//         dewp: { dbKey: 'DEWP', guiText: 'DEWPOINT TEMPERATURE', unit: 'degC' },
//         wtmp: { dbKey: 'WTMP', guiText: 'SEA SURFACE TEMPERATURE', unit: 'degC' },
//         atmp: { dbKey: 'ATMP', guiText: 'AIR TEMPERATURE', unit: 'degC' },
//         ptdy: { dbKey: 'PTDY', guiText: 'PRESSURE TENDENCY', unit: 'hPa' },
//         pres: { dbKey: 'PRES', guiText: 'SEA LEVEL PRESSURE', unit: 'hPa' },
//         mwd: { dbKey: 'MWD', guiText: 'WAVE DIRECTION @ DOMINANT PERIOD', unit: 'DTN' },
//         apd: { dbKey: 'APD', guiText: 'AVERAGE WAVE PERIOD', unit: 'sec' },
//         dpd: { dbKey: 'DPD', guiText: 'DOMINANT WAVE PERIOD', unit: 'sec' },
//         wvht: { dbKey: 'WVHT', guiText: 'SIGNIFICANT WAVE HEIGHT', unit: 'm' },
//         gst: { dbKey: 'GST', guiText: 'PEAK 5 OR 8 SECOND GUST SPEED', unit: 'm/s' },
//         wspd: { dbKey: 'WSPD', guiText: 'WIND SPEED', unit: 'm/s' },
//         wdir: { dbKey: 'WDIR', guiText: 'WIND DIRECTION', unit: 'DTN' },
//         mm: { dbKey: 'mm', guiText: 'MINUTES', unit: 'minutes' },
//         hh: { dbKey: 'hh', guiText: 'HOURS', unit: 'hours' },
//         dd: { dbKey: 'DD', guiText: 'DAY', unit: 'day' },
//         MM: { dbKey: 'MM', guiText: 'MONTH', unit: 'month' },
//         YYYY: { dbKey: 'YYYY', guiText: 'YEAR', unit: 'year' }

//     }

//     window.removeBeacon = function(p_beaconId) {

//         $.ajax({
//             url: domain + '/api/beacon/remove/' + p_beaconId,
//             method: 'POST'
//         }).then(function(data, textStatus, jqXHR) {
//             console.log('success post' + data);
//             alert('Beacon: '+p_beaconId+' has been removed.');
//             refreshUserBeacons();
//         }, function() {
//             console.log('no beacon info found for: ' + p_beaconId)
//             refreshUserBeacons();
//             //alert('You need to download the server seed and start it to call this API');
//         });

//     }

//     window.saveBeacon = function(p_beaconId) {

//         $.ajax({
//             url: domain + '/api/beacon/save/' + p_beaconId,
//             method: 'POST'
//         }).then(function(data, textStatus, jqXHR) {
//             console.log('success post' + data);
//             alert('Beacon: '+p_beaconId+' has been saved.');
//         }, function() {
//             console.log('no beacon info found for: ' + p_beaconId)
//                 //return null;
//                 //alert('You need to download the server seed and start it to call this API');
//         });

//     }

//     var _beaconSchemaByDbKey = function() {
//         var _newObj = {};
//         for (key in _beaconSchema) {

//             var _theDbKey = _beaconSchema[key].dbKey;
//             _newObj[_theDbKey] = _beaconSchema[key];

//         }
//         return _newObj;
//     }()



//     $('.btn-login').click(function(e) {
//         e.preventDefault();
//         lock.showSignin(function(err, profile, token) {
//             if (err) {
//                 // Error callback
//                 console.log('There was an error');
//                 alert('There was an error logging in');
//             } else {
//                 // Success calback

//                 // Save the JWT token.
//                 localStorage.setItem('userToken', token);

//                 // Save the profile
//                 userProfile = profile;

//                 $('.login-box').hide();
//                 $('.logged-in-box').show();
//                 $('.nickname').text(profile.nickname);
//                 $('.nickname').text(profile.name);
//                 $('.avatar').attr('src', profile.picture);

//                 //to refresh the map for some reason needs to be used with jquery show/hide
//                 mapid.invalidateSize(true);
//                 getUserBeacons();
//             }
//         });
//     });

//     $.ajaxSetup({
//         'beforeSend': function(xhr) {
//             if (localStorage.getItem('userToken')) {
//                 console.log('token is: ' + localStorage.getItem('userToken'))
//                 xhr.setRequestHeader('Authorization',
//                     'Bearer ' + localStorage.getItem('userToken'));
//             }
//         }
//     });

//     function plotBeacon(p_beaconObject) {

//         console.log(p_beaconObject)

//         var _lat = parseFloat(p_beaconObject[_beaconSchema.lat.dbKey]);
//         console.log("_lat is: " + _lat);
//         var _lon = parseFloat(p_beaconObject[_beaconSchema.lon.dbKey]);
//         if (_lat === 'NaN' || _lon === 'NaN') {
//             console.log('plotBeacon: lat or lon is undefined');
//             return;
//         }

//         var _newArr = [];
//         _newArr.push(_lat);
//         _newArr.push(_lon);

//         console.log(_newArr);

//         // var marker = L.marker(_newArr);
//         var _str = p_beaconObject[_beaconSchema.id.dbKey];
//         if (!_str) {
//             console.log('plotBeacon: beaconObject #STN is blank')
//         }


//         //console.log(_beaconKey)
//         var _beaconId = p_beaconObject[_beaconSchema.id.dbKey];
//         var _object = {};
//         _object[_beaconSchema.id.dbKey] = _beaconId;
//         var marker = L.marker(_newArr, _object);        
//         var _fahrenheit = p_beaconObject[_beaconSchema.wtmp.dbKey] * 9 / 5 + 32
//         marker.bindPopup('' + _beaconId + ' <br><a href=\'#/\' onclick=window.saveBeacon(\'' + _beaconId + '\')>Save</a><br> <a href=\'#/\' onclick=window.removeBeacon(\'' + _beaconId + '\')>Remove</a><br><span>Temperature: '+_fahrenheit+'&deg; F</span>')
//         marker.addTo(_beaconLayer).on('click', beaconMarkerClicked);


//     }


//     function beaconMarkerClicked() {
//         console.log('hey we here')
//         console.log(this.options);
//         console.log('key is: ' + _beaconSchema.id.dbKey);
//         var _stn = _beaconSchema.id.dbKey;
//         getBeaconInfo(this.options[_stn], populateBottomTable);
//         return false;


//     }

//     function populateBottomTable(p_beaconObject) {

//         var _table = document.getElementById('currentBeaconInfo');
//         removeChildNodes(_table);
//         var _thead = create('thead');
//         var _tbody = create('tbody');
//         var _theadRow = create('tr');
//         var _tr = create('tr');
//         for (key in _beaconSchemaByDbKey) {
            
//             var _val = p_beaconObject[key];
            
            
//             //append to the header           
//             var _th = create('th');
//             _th.innerText = _beaconSchemaByDbKey[key].guiText;
//             _theadRow.appendChild(_th);
            

            
//             var _tdVal = create('td');
            
            
//             if(_val === 'MM'){
//                 _val = 'N/A';
//             }
//             //_tdKey.innerText = _beaconSchemaByDbKey[key].guiText;
//             _tdVal.innerText = _val + ' ' + _beaconSchemaByDbKey[key].unit;
//             //_tr.appendChild(_tdKey);
//             _tr.appendChild(_tdVal);
            
//             console.log(_beaconSchemaByDbKey[key].guiText + ' : ' + p_beaconObject[key] + ' ' + _beaconSchemaByDbKey[key].unit);
//         }
//         _thead.appendChild(_theadRow);
//         _table.appendChild(_thead);
//         _tbody.appendChild(_tr);
//         _table.appendChild(_tbody);

//     }

//     function refreshUserBeacons() {
//         clearMarkersFromMap();
//         getUserBeacons();
//     }


//     function getBeaconInfo(p_beaconId, p_cb) {
//         if (!p_beaconId) {
//             console.log('getBeaconInfo: no beacon passed');
//             return;
//         }
//         if ((p_cb) && !(typeof p_cb === 'function')) {
//             console.log('getBeaconInfo: passed callback is not a function');
//             return;
//         }

//         console.log('p_beaconId is ' + p_beaconId);
//         $.ajax({
//             url: domain + '/api/beacon/' + p_beaconId,
//             method: 'GET'
//         }).then(function(data, textStatus, jqXHR) {
//             console.log(data);
//             //plotBeacon(data);
//             p_cb(data);
//         }, function() {
//             console.log('no beacon info found for: ' + p_beaconId)
//             return null;
//             //alert('You need to download the server seed and start it to call this API');
//         });
//     }

//     function clearMarkersFromMap() {
//         mapid.removeLayer(_beaconLayer);
//         _beaconLayer = L.featureGroup().addTo(mapid);
//     }

//     function getAllBeacons() {
//         // Just call your API here. The header will be sent
//         $.ajax({
//             url: domain + '/api/beacons',
//             method: 'GET'
//         }).then(function(data, textStatus, jqXHR) {
//             console.log(jqXHR);
//             console.log(data);
//             clearMarkersFromMap();
//             for (var _i = 0; _i < data.length; _i++) {
//                 plotBeacon(data[_i])
//             }

//             //alert('The request to the secured enpoint was successfull');
//             console.log(data);

//         }, function() {
//             //alert('You need to download the server seed and start it to call this API');
//         });

//     }

//     function getUserBeacons() {
//         console.log('get user beacons')
//             // Just call your API here. The header will be sent
//         $.ajax({
//             url: domain + '/api/user/beacons',
//             method: 'GET'
//         }).then(function(data, textStatus, jqXHR) {
//                 console.log(jqXHR);
//                 console.log(data);
//                 clearMarkersFromMap();
//                 if (!(data.success === 'false')) {
//                     for (var _i = 0; _i < data.length; _i++) {
//                         getBeaconInfo(data[_i], plotBeacon)
//                     }
//                 }
//                 //alert('The request to the secured enpoint was successfull');
//                 console.log(data);

//             },
//             function() {
//                 //alert('You need to download the server seed and start it to call this API');
//             });

//     }

//     window.ajaxRequest = function(p_method, p_url, p_cb) {
//         if (p_method != 'GET' && p_method != 'POST') {
//             console.log('passed method is not GET OR POST');
//             return;
//         }

//         $.ajax({
//             url: domain + p_url,
//             method: p_method,
//         }).then(function(data, textStatus, jqXHR) {
//                 if (p_cb && (typeof p_cb === 'function')) {
//                     p_cb(data);
//                 }

//             },
//             function() {
//                 //alert('You need to download the server seed and start it to call this API');
//             });
//     }

//     function findPlace(p_placeString, p_radius) {
//         if(!p_placeString){
//             console.log('you should enter a place like 92648');
//         }
//         if(!p_radius){
//             p_radius = 20;
//         }
//         $.get('https://nominatim.openstreetmap.org/search?format=json&q=+' + p_placeString + '').done(function(data) {
//             console.log(data);
//             if (data.length > 0) {
//                 searchByLonLat(data[0].lon, data[0].lat, p_radius);
//             } else {
//                 console.log('no place found');
//             }
//         })


//     };

//     $('.btn-search-place').click(function(e) {
//         findPlace($("#place-input").val(), $("#place-radius-input").val());
//         e.preventDefault();
//     });

//     $('.btn-api-user-beacons').click(function(e) {
//         getUserBeacons();
//         e.preventDefault();

//     });


//     $('.btn-api-all-beacons').click(function(e) {
//         getAllBeacons();
//         e.preventDefault();

//     });
//     $('.btn-logout').click(function(e) {
//         window.location = 'https://' + Auth0Variables.AUTH0_DOMAIN + '/v2/logout?returnTo=' + encodeURIComponent(domain) + '&access_token=' + localStorage.userToken;


//     })

//     function searchByLonLat(_lon, _lat, _radius) {
//         $.ajax({
//             url: domain + '/api/beacon/location' + '?lon=' + _lon + '&lat=' + _lat + '&radius=' + _radius,
//             method: 'GET'
//         }).then(function(data, textStatus, jqXHR) {
//             console.log(jqXHR);
//             console.log(data);
//             clearMarkersFromMap();
//             for (var _i = 0; _i < data.length; _i++) {
//                 plotBeacon(data[_i].data);
//             }

//             //alert('The request to the secured enpoint was successfull');
//             console.log(data);

//         }, function() {
//             //alert('You need to download the server seed and start it to call this API');
//         });
//     }

//     $('.search-lon-lat').click(function(e) {

//         var _lat = parseFloat($('#lat-input').val());
//         var _lon = parseFloat($('#lon-input').val());
//         var _radius = parseInt($('#radius-input').val());


//         if (!_lat || !_lon) {
//             alert('you did not enter lon or lat?');
//         }
//         if (!_radius) {
//             console.log('radius-input not entered, will use default radius');
//         }

//         searchByLonLat(_lon, _lat, _radius);
//         e.preventDefault();
//     });


//     $('.btn-api-update').click(function(e) {

//         $.ajax({
//             url: domain + '/update',
//             method: 'POST'
//         }).then(function(data, textStatus, jqXHR) {
//             console.log(jqXHR);
//             console.log(data);

//             //alert('The request to the secured enpoint was successfull');
//             console.log(data);

//         }, function() {
//             //alert('You need to download the server seed and start it to call this API');
//         });
//     });

//     mapid.on('click', function(e) {
//         searchByLonLat(e.latlng.lng, e.latlng.lat, 50);
// });


// });
