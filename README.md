
	
#YeahBuoy!


### Intro

Hi there!  My name is Sean Finkel and this is my coding challenge for Surfline!
######Thank you for this opportunity! 

I wanted to give you an overview of my approach for the challenge and explain why went the way I did.  The challenge was to: 



To start off, I wanted to:

* Attempt to recreate the NOAA APIs returning different sorts of XML buoy data into a more concise REST-ful JSON API (this seems to be the current trend i.e. Google Image, GIPHY, etc. and it seems to make data parsing easier to manage)
* Make the API so it could be expanded upon
* Recreate the geospatial features as well, but let Mongodb do the work (lightens the load and lines of code).  
* Make it something that other might possibly WANT to use and make it user friendly.
* Keep it fun

Logically, I am sure things could be done much differently and I look forward to hearing those suggestions.

### Requirements

* As a user I want to view a list of buoys within 100 nautical miles of 40N and 73W

* As a user I want to favorite a specific buoy station.

* As a user I want to list all my favorite buoy stations (at minimum a name, station id, & latitude/longitude) and their current readings.

### Investiation Process

The first thing I started to look in to was the different APIs available for receiving buoy data for NOAA, keeping in mind that each user should be able to save his/her own beacon data.  I took a look at the XML data coming back from the RSS feeds and thought about the possiblity of creating a server that parsed the data, or just forwarded it a proxy to the requesting client.  I decided against this.  

Why?  

I found that a lot of the RSS feeds and XML data had styles (i.e. '\<strong>') inline with the data and I found that while I could programatically parse the desired data out, or just send to the client and have it use/parse, I thought a developer would really just prefer the bare minimum.  For example, here's a beacon ID I want to know about, give me it's data.  (i.e. WATER TEMP : 70 degC).  I was lucky enough to come across NOAA's flat text file (TSV) indicating all beacon data and updating every 5 minutes.  (http://www.ndbc.noaa.gov/data/latest_obs/latest_obs.txt).  I figured I could parse this somehow and get this in to a database which the REST API could access.

After I decided I wanted to recreate the APIs, I started looking in to the fact that I would need to also recreate the GEO data as well, as we would need to list buoys 100NM within a given LON/LAT.  To do this, I leveraged the power of a modern database, MongoDb, as I found the GEO examples relatively easy to understand and figured it would be pretty easy/quick to get the TSV data in to it.

The last thing (in terms of Back-End) I took a look in to before getting started, was how I would store the user's data securely so that user1 could not take advantage of user2's data.  I thought that it was a good time to experiment with JSON web tokens which I have heard so much about and have used in other APIs, but had never had a chance to use first-hand acting as the API provider. It seemed like a really solid way to do this stuff securely.  Witht that decided, I looked at several methods of account creation, password storing, token creation and decided I could just use auth0.com to do it all as they provide a lot of what I was going to go through manually and for free.

In terms of Front-End work, I decided I would utilize my custom build tooling I have used before.  I use Gulp with a combination of a ton of other tools under the hood to get the work done fast.  I debated using React and just didn't want to spend too much time learning how to integrate the auth0 library in to it and make a whole new build system.  I decided to just go with what I know.



### Development Process

Development of this project was about 70% Back-End and about 30% Front-End.  I started with the Back-End as planned, and got a Node.JS server up and running with a Mongoose connector to a MongoDB database hosted at mlab.com.  I created the schemas for beacon data and locations (in retrospect, I should have combined this I think) and found a way to get the NOAA TSV file data inputed in to the database.  I stated modeling the APIs in a way that I thought made sense to the use-case of this project, but also, a potential use-case for future use.  I will list the APIs down below.

The integration of the JSON Web Tokens was troublesome at first, but became much easier as development went along.  I was finally able to reliably have users login and had many friends and family members help test and try to break it.  I ended up finding an interesting bug where the converter plugin used in TSV->Mono had to be newly constructed every time the conversion took place.  Otherwise, it would convert Ten times and crash the server as Node only allows 10 default references or something like that and then it throws and error ha.

It was also my first time using Heroku to deploy a Node.JS application and there were definitely some build issues happening there along the way + a new learning curve.  It was recommended by the auth0 team to use their webpack module for their lock product, but I found that using webpack to compile modules resulted in a much larger JS file than my own Gulp build created which concats all JS files.  While I appreciate keeping variables and functions out of the global scope and the modularity that webpack provides by pulling module resources, I could not sacrifice a large output file size impacting page load for those benefits.  I ended up scrapping webpack and integrated their JS dependencies in to my own build method.  After doing that, I saw a smaller file = much better page load time.

Another thing that caused a hiccup along the way, was almost missing the requirement about being able to see the Station Name.  The TSV file that NOAA provides has all the information asscociated to their beacons stored by beacon ID.  The only way I could see to get the actual name/description of the Station Name (i.e. 'NDBC - Station LUIT2 - 8771972 - San Luis Pass, TX Observations') was to query NOAA's XML again, which I was trying to avoid as much as possible.  To fix this, I ended up creating an API which could pull down the RSS feed of a specific beacon, parse the XML, and return (in JSON format) the beacon details.  I am not in love with this and would go back and have the server->db do some extra work to store everything it could get on each beacon.  This would allow the data to stay in my DB preventing an additional request to NOAA.

All said and done, I hope most bugs have been sorted out.  I would love to add unit testing / functional testing in the future.  That should also be ready to go with the custom gulp build template.  I would also like to jshint/jsdoc the code. 

###Usage

To use, login with either auth0, Facebook, Google.  Click the map to show beacons within a 20 Nautical Mile radius.  You can also search by LON/LAT OR Zip Code (really any search) using the Search dropdown. The map should zoom you in to the last beacon found.  BE CAREFUL:  If you have used the search to find beacons, and you click the map again in a location), the beacons on the map will be replaced with ones 20 NM from your clicked location.

If no beacons are found, you should not see anything.  If beacons are found, you should also see them highlighed a color based on their water temperature range.  From colder water to warmer water the colors are respectively: Purple -> Blue -> Light Blue -> Orange -> Red.  White is the color of the beacon when the temperature is unknown.

When a beacon is clicked, the information for it will be displayed on the page as well.  If a picture of the beacon is available from NOAA, it will be displayed as well.

Right now, the server is not auto-updating the data from NOAA.  I thought it would be better not to hammer their site with requests every five minutes, although, this could be implemented rather quickly.  

To update the data manually, you can issue the /update API command below.  This will go fetch all the new data.  <strong>Keep in mind</strong>, this will also possibly change functionality of the page for about a minute as the database is being written to.

Clicking the button 'User Beacons' will show you all of the beacons you have saved.

Logout will kill the token for your current account and log you out.

To run the server, `node server.js` or even better `nodemon server.js` if you have nodemon installed.  You can install with `npm install nodemon -g`


###APIs Endpoints 
Here are all the API endpoints found at:

http://vast-coast-33220.herokuapp.com

###TOKENIZED (if using url always add token=yourJWT)
#### /api/beacons/ (GET)
Action: Get information for all beacons

Success : Returns an array of beacon objects
Fail : Returns null


#### /api/beacon/save/:beacon (POST)
Action: Saves a user's beacon
(Replace ':beacon' with the beacon id of your choice)
Returns JSON - {success: true/false}

#### /api/beacon/remove/:beacon (POST)
Action: Removes a user's saved beacon
(Replace ':beacon' with the beacon id of your choice)
Returns JSON - {success: true/false}

#### /user/beacons/ (GET)
Action: Retrieves an array of a user's favorite beacons

Success : Returns an array of beacons associated with the user
Fail : Returns null

#### /beacon/location/ (GET)
Action: Finds beacons within a longitude,latitude, and optional radius (in 
Nautical Miles)

Example: ?lon=yourLon&lat=yourLat&radius=20

Success:  Returns an array of beacon objects within that area
Fail: Returns null


#### /beacon/:beacon (GET)

Action: Finds information about a specific beacon
(Replace ':beacon' with the beacon id of your choice)

Fail: JSON {success: false}


#### /xml/beacon (GET)
Action: Retrieves information about a beacon using NOAA's RSS feeds

Using parameter = beacon=:beacon
(Replace ':beacon' with the beacon id of your choice)
Example: /api/xml/beacon?beacon=PTBM6

Success: JSON of XML meta data
Fail: JSON {success: false}



###Server Helpers (token not needed)

Helpfulfor development, but should take out for production

#### /update (POST)
Action: Tells the server to go update with the latest TSV DATA 
(http://www.ndbc.noaa.gov/data/latest_obs/latest_obs.txt)

#### /verify (POST)
Action: Decodes a user's JWT

Success: Returns the decoded token
Fail: JSON {success: false}




#####Anwyay, I hope you enjoy the project!

###Thanks again for the opportunity!



### Version
0.0.1

### Tech Involved

* [node.js] - evented I/O for the backend
* [npm] - package manager
* [Gulp] - the streaming build system (and many gulp plugins! ..see: package.json)
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [sass] - Syntactically Awesome Style Sheets
* [jsdoc] - JavaScript documentation generator
* [jshint] - a JavaScript Code Quality Tool
* [gitlab] - Git repository management
* [MongoDB] - Free and open-source cross-platform document-oriented database
* [mlab] - Database-as-a-Service for MongoDB
* [auth0] - A universal identity platform for customers, employees and partners


### Installation (if you want to make a build)

In order to make a build, you need to clone the repo, run `npm install` and use gulp to build a development build.


### Trying it out (if you don't want to make a build)

I have put a development build all wrapped up in a src.zip file at this location:

```
$ ./build/dev/src.zip
```

Just unzip the file.

### Building the 'fun' way

If you want to try out the development stuff, here are some commands I use regularly.  First, in a terminal, change to the directory and use one of these commands:

##### Development

```
gulp build-dev
```

* A new development build is created


```
gulp watch-dev
```

* Watches the /src folder for changes
* When a file is changed, a new development build is created
* A page load is triggered
* If this is the first time the command is run, it will try to open the build in the browser



The commands above should generate a build based on your current /src files any time a change is made or even if they are saved.  If you are using a watch command, like `gulp watch-dev`, you will see the page reload everytime a change is made as well.

This way, you know if you are failing pretty quickly.  If you have a JS error, you may need to reload the page depending on what point the code breaks.  Majority of the time it will reload automatically.

So like previously mentioned, when you save a file, a build will be placed in `./build/dev/src/`.  On the first time the command is run, Gulp will try to open your web browser (or open a new tab) with the build.  After that, the page will use livereload to show live changes.

**CSS/SASS:** Gulp compiles the sass and css files and concatenates them in to one for the final build.  An autoprefixer is run on the files to make sure I didn't leave out vendor prefixes.

**JS**: Gulp takes all the JS right now and concatenates it in to one file. 




   
   [node.js]: <http://nodejs.org>
   [npm]: <https://www.npmjs.com>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [Gulp]: <http://gulpjs.com>
   [sass]: <http://sass-lang.com/>
   [gulp-livereload]: <https://www.npmjs.com/package/gulp-livereload>
   [gulp-html-replace]: <https://www.npmjs.com/package/gulp-html-replace>
   [gulp-sass]: <https://www.npmjs.com/package/gulp-sass>
   [jsdoc]: <http://usejsdoc.org/>
   [jshint]: <http://jshint.com/>
   [gitlab]: <https://gitlab.com/>
   [mlab]: <https://mlab.com/>
   [MongoDB]: <https://www.mongodb.com>
   [auth0]: <https://auth0.com>
   
  





